<?php

return [
    // 
    // Core Config
    // =========================================================================
    //"DATABASE_HOST" => "localhost",
    "DATABASE_HOST" => "us-cdbr-iron-east-02.cleardb.net",
    //"DATABASE_NAME" => "delicata",
    "DATABASE_NAME" => "heroku_93b8225a848ec49",
    //"DATABASE_USERNAME" => "root",
    //"DATABASE_PASSWORD" => "",
    "DATABASE_USERNAME" => "b7a55d865f9483",
    "DATABASE_PASSWORD" => "552fc53d",
    // 
    // Cookie Config
    // =========================================================================
    "COOKIE_DEFAULT_EXPIRY" => 604800,
    "COOKIE_USER" => "user",
    "" => "",
    // 
    // Session Config
    // =========================================================================
    "SESSION_ERRORS" => "errors",
    "SESSION_FLASH_DANGER" => "danger",
    "SESSION_FLASH_INFO" => "info",
    "SESSION_FLASH_SUCCESS" => "success",
    "SESSION_FLASH_WARNING" => "warning",
    "SESSION_TOKEN" => "token",
    "SESSION_TOKEN_TIME" => "token_time",
    "SESSION_USER" => "user",
    "" => "",
    // 
    // Texts Config
    // =========================================================================
    "TEXTS" => [
        // 
        // Login Model Texts
        // =====================================================================
        "LOGIN_INVALID_PASSWORD" => "El correo y la contraseña no coinciden!",
        "LOGIN_USER_NOT_FOUND" => "El correo ingresado no se encuentra registrado!",
        "" => "",
        // 
        // Register Model Texts
        // =====================================================================
        "REGISTER_USER_CREATED" => "Su cuenta ha sido creada exitosamente!",
        "" => "",
        // 
        // User Model Texts
        // =====================================================================
        "USER_CREATE_EXCEPTION" => "Hubo un problema creando su cuenta!",
        "USER_UPDATE_EXCEPTION" => "Hubo un problema actualizando su cuenta!",
        "" => "",
        // 
        // Input Utility Texts
        // =====================================================================
        "INPUT_INCORRECT_CSRF_TOKEN" => "Cross-site request forgery verification failed!",
        "" => "",
        // 
        // Validate Utility Texts
        // =====================================================================
        "VALIDATE_FILTER_RULE" => "%ITEM% no es un %RULE_VALUE% valido!",
        "VALIDATE_MISSING_INPUT" => "Unable to validate %ITEM%!",
        "VALIDATE_MISSING_METHOD" => "Unable to validate %ITEM%!",
        "VALIDATE_MATCHES_RULE" => "%RULE_VALUE% must match %ITEM%.",
        "VALIDATE_MAX_CHARACTERS_RULE" => "%ITEM% solo puede tener un maximo de %RULE_VALUE% caracteres.",
        "VALIDATE_EQUAL_CHARACTERS_RULE" => "%ITEM% tiene que tener %RULE_VALUE% caracteres.",
        "VALIDATE_NUM_CHARACTERS_RULE" => "%ITEM% tiene que ser un numero.",
        "VALIDATE_MIN_CHARACTERS_RULE" => "%ITEM% debe tener un minimo de %RULE_VALUE% caracteres.",
        "VALIDATE_REQUIRED_RULE" => "%ITEM% es obligatorio!",
        "VALIDATE_UNIQUE_RULE" => "%ITEM% ya existe.",
        "" => "",
        // 
        // Texts
        // =====================================================================
        "" => "",
    ],
    // 
    // Config
    // =========================================================================
    "" => "",
];
