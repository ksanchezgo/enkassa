<?php

namespace App\Controller;

use App\Core;
use App\Model;
use App\Utility;

/**
 * Index Controller:
 *
 * @author Andrew Dyer <andrewdyer@outlook.com>
 * @since 1.0
 */
class Nosotros extends Core\Controller {

    /**
     * Index: Renders the index view. NOTE: This controller can only be accessed
     * by authenticated users!
     * @access public
     * @example index/index
     * @return void
     * @since 1.0
     */
    
    public function index() {

        // Check that the user is authenticated.
        //Utility\Auth::checkAuthenticated();

        // Get an instance of the user model using the ID stored in the session. 
        /*$userID = Utility\Session::get(Utility\Config::get("SESSION_USER"));
        if (!$User = Model\User::getInstance($userID)) {
            Utility\Redirect::to(APP_URL);
        }*/

        $userID = -1;
       $userID = Utility\Session::get(Utility\Config::get("SESSION_USER"));
       $this->View->addCSS("css/index.css");
       $this->View->addJS("js/index.jquery.js");
        if ($userID<1) {

            $this->View->render("nosotros/index", [
                "title" => "Nosotros"
            ]);
            
        }else{
            
            $User = Model\User::getInstance($userID);
            $this->View->render("nosotros/index", [
                "title" => "Nosotros",
                "user" => $User->data()
            ]);
        }
    }

}
