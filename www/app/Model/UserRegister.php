<?php

namespace App\Model;

use Exception;
use App\Utility;


/**
 * User Register Model:
 *
 * @author Andrew Dyer <andrewdyer@outlook.com>
 * @since 1.0.2
 */
class UserRegister {

    /** @var array The register form inputs. */
    private static $_inputs = [
        "nombre" => [
            "required" => true
        ],
        "apellido" => [
            "required" => true
        ],
        "dni" => [
            "required" => true,
            "num_characters" => 1,
            "eql_characters" => 8
        ],
        "correo" => [
            "filter" => "correo",
            "required" => true,
            "unique" => "users"
        ],
        "contrasena" => [
            "min_characters" => 6,
            "required" => true
        ],
        "repetir_contrasena" => [
            "matches" => "contrasena",
            "required" => true
        ],
    ];

    /**
     * Register: Validates the register form inputs, creates a new user in the
     * database and writes all necessary data into the session if the
     * registration was successful. Returns the new user's ID if everything is
     * okay, otherwise turns false.
     * @access public
     * @return boolean
     * @since 1.0.2
     */
    public static function register() {

        // Validate the register form inputs.
        if (!Utility\Input::check($_POST, self::$_inputs)) {
            return false;
        }
        try {

            // Generate a salt, which will be applied to the during the password
            // hashing process.
            $salt = Utility\Hash::generateSalt(32);

            // Insert the new user record into the database, storing the unique
            // ID which will be returned on success.
            $User = new User;
            $userID = $User->createUser([
                "correo" => Utility\Input::post("correo"),
                "nombre" => Utility\Input::post("nombre"),
                "dni" => Utility\Input::post("dni"),
                "contrasena" => Utility\Hash::generate(Utility\Input::post("contrasena"), $salt),
                "salt" => $salt,
                "apellido" => Utility\Input::post("apellido")
            ]);

            // Write all necessary data into the session as the user has been
            // successfully registered and return the user's unique ID.
            $name = Utility\Input::post("nombre");
            $correo = Utility\Input::post("correo");

            $correofrom = "kesanchezgo@gmail.com";
            $correoto = $correo;

            /*
            $from = new \SendGrid\Email(null, $correofrom);
            $subject = "Oficina de Enkassa - Alerta";
            $to = new \SendGrid\Email(null, $correoto);
            $content = new \SendGrid\Content("text/plain", "Bienvenido a Enkassa $name !!, ya puedes usar nuestro sistema para buscar un lugar donde vivir o empezar un negocio alquilando tu propia casa.");
            $mail = new \SendGrid\Mail($from, $subject, $to, $content);

            $apiKey = getenv('SENDGRID_API_KEY');
            $sg = new \SendGrid($apiKey);

            $response = $sg->client->mail()->send()->post($mail);*/


            $email = new \SendGrid\Mail\Mail(); 
            $email->setFrom($correofrom, "Example User");
            $email->setSubject("Sending with Twilio SendGrid is Fun");
            $email->addTo($correoto, "Example User");
            $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
            $email->addContent(
                "text/html", "<strong>and easy to do anywhere, even with PHP</strong>"
            );
            $sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
            try {
                $response = $sendgrid->send($email);
                print $response->statusCode() . "\n";
                print_r($response->headers());
                print $response->body() . "\n";
            } catch (Exception $e) {
                echo 'Caught exception: '. $e->getMessage() ."\n";
            }




            Utility\Flash::success(Utility\Text::get("REGISTER_USER_CREATED"));
            return $userID;
        } catch (Exception $ex) {
            Utility\Flash::danger($ex->getMessage());
        }
        return false;
    }

}
