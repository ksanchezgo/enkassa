
<section class="intro-single">
<div class="container">
    <div class="row">
        <div class="col-md-4 col-lg-4"></div>
        <div class="col-md-4 col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">¡Únete a Nosotros!</h3>
                </div>
                <br>
                <div class="panel-body">
                    <form  class="form-a" action="<?= $this->makeUrl("register/_register"); ?>" method="post">
                        <div class="form-group">
                            
                            <input type="text" id="forename-input" class="form-control form-control-lg form-control-a" name="nombre" placeholder="Nombres"/>
                        </div>
                        <div class="form-group">
                            
                            <input type="text" id="surname-input" class="form-control form-control-lg form-control-a" name="apellido" placeholder="Apellidos"/>
                        </div>
                        <div class="form-group">
                            
                            <input type="text" id="dni-input" class="form-control form-control-lg form-control-a" name="dni" placeholder="Numero de DNI" />
                        </div>                         
                        <div class="form-group">
                            
                            <input type="text" id="email-input" class="form-control form-control-lg form-control-a" name="correo" placeholder="Correo electrónico"/>
                        </div>
                        <div class="form-group">
                            
                            <input type="password" id="password-input" class="form-control form-control-lg form-control-a" name="contrasena" placeholder="Contraseña"/>
                        </div>
                        <div class="form-group">
                            
                            <input type="password" id="password-repeat-input" class="form-control form-control-lg form-control-a" name="repetir_contrasena" placeholder="Repetir contraseña"/>
                        </div>
                        <br>
                        <input type="hidden" name="csrf_token" value="<?php echo App\Utility\Token::generate(); ?>" />
                        <button type="submit" class="btn btn-b">Registrarse</button>
                        <a href="<?= $this->makeURL("login"); ?>" class="btn">Cancelar</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>