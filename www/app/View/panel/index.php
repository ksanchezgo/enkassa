<div class="container">
 <br>
<section class="section-services section-t8">
    <?php if (isset($this->user)) : ?>
        <div class="jumbotron">
            <h1>Bienvenido, <?= $this->escapeHTML($this->user->nombre . " " . $this->user->apellido); ?>!</h1>
            <p>...</p>
            <p>
                <a class="btn btn-default btn-lg" href="<?= $this->makeURL("profile"); ?>" role="button">Mi perfil</a>
                <a class="btn btn-primary btn-lg" href="<?= $this->makeURL("login/logout"); ?>" role="button">Salir</a>
            </p>
        </div>
    <?php endif; ?>
</section>
</div>