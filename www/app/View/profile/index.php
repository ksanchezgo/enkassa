<?php if (isset($this->data)): ?>
	<br>
	<section class="section-services section-t8">
    <div class="container">
        <div class="jumbotron">
            <h1><?= $this->escapeHTML($this->data->name); ?></h1>
        </div>
    </div>
	</section>
<?php endif; ?>