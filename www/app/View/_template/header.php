 <?php

use App\Utility\Config;
use App\Utility\Flash;
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= $this->escapeHTML($this->title . " - " . APP_NAME); ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="" rel="shortcut icon">
        <!--<link href="<?= $this->makeURL("css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css"/>-->
        <link href="<?= $this->makeURL("font/font-awesome.min.css"); ?>" rel="stylesheet" type="text/css"/>
        <!-- Favicons -->
        <link href="<?= $this->makeURL("img/enkassa-icon.png"); ?>" rel="icon">
        <link href="<?= $this->makeURL("img/enkassa-icon.png"); ?>" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

        <!-- Bootstrap CSS File -->
        <link href="<?= $this->makeURL("lib/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet">

        <!-- Libraries CSS Files -->
        <link href="<?= $this->makeURL("lib/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet">
        <link href="<?= $this->makeURL("lib/animate/animate.min.css"); ?>" rel="stylesheet">
        <link href="<?= $this->makeURL("lib/ionicons/css/ionicons.min.css"); ?>" rel="stylesheet">
        <link href="<?= $this->makeURL("lib/owlcarousel/assets/owl.carousel.min.css"); ?>" rel="stylesheet">
        <!-- Main Stylesheet File -->
        <link href="<?= $this->makeURL("css/style.css"); ?>" rel="stylesheet">

        <?= $this->getCSS(); ?>
        
    </head>
    <body style="background-color: #fafafa">

      <div class="click-closed"></div>
        <!--/ Form Search Star /-->
        <div class="box-collapse">
          <div class="title-box-d">
            <h3 class="title-d">Buscar Propiedad</h3>
          </div>
          <span class="close-box-collapse right-boxed ion-ios-close"></span>
          <div class="box-collapse-wrap form">
            <form class="form-a">
              <div class="row">
                <div class="col-md-12 mb-2">
                  <div class="form-group">
                    <label for="Type">Keyword</label>
                    <input type="text" class="form-control form-control-lg form-control-a" placeholder="Keyword">
                  </div>
                </div>
                <div class="col-md-6 mb-2">
                  <div class="form-group">
                    <label for="Type">Type</label>
                    <select class="form-control form-control-lg form-control-a" id="Type">
                      <option>All Type</option>
                      <option>For Rent</option>
                      <option>For Sale</option>
                      <option>Open House</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6 mb-2">
                  <div class="form-group">
                    <label for="city">City</label>
                    <select class="form-control form-control-lg form-control-a" id="city">
                      <option>All City</option>
                      <option>Alabama</option>
                      <option>Arizona</option>
                      <option>California</option>
                      <option>Colorado</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6 mb-2">
                  <div class="form-group">
                    <label for="bedrooms">Bedrooms</label>
                    <select class="form-control form-control-lg form-control-a" id="bedrooms">
                      <option>Any</option>
                      <option>01</option>
                      <option>02</option>
                      <option>03</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6 mb-2">
                  <div class="form-group">
                    <label for="garages">Garages</label>
                    <select class="form-control form-control-lg form-control-a" id="garages">
                      <option>Any</option>
                      <option>01</option>
                      <option>02</option>
                      <option>03</option>
                      <option>04</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6 mb-2">
                  <div class="form-group">
                    <label for="bathrooms">Bathrooms</label>
                    <select class="form-control form-control-lg form-control-a" id="bathrooms">
                      <option>Any</option>
                      <option>01</option>
                      <option>02</option>
                      <option>03</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-6 mb-2">
                  <div class="form-group">
                    <label for="price">Min Price</label>
                    <select class="form-control form-control-lg form-control-a" id="price">
                      <option>Unlimite</option>
                      <option>$50,000</option>
                      <option>$100,000</option>
                      <option>$150,000</option>
                      <option>$200,000</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <button type="submit" class="btn btn-b">Search Property</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!--/ Form Search End /-->

        <!--/ Nav Star /-->
        <nav class="navbar navbar-default navbar-trans navbar-expand-lg fixed-top">
          <div class="container">
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
              aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
              <span></span>
              <span></span>
              <span></span>
            </button>
            <a class="navbar-brand text-brand" href="<?= $this->makeURL("inicio"); ?>"><!--Estate--><img style="padding-bottom: 7%"width="17%" src="<?= $this->makeURL("img/enkassa-icon.png"); ?>"><span class="color-b">Enkassa</span></a>
            <button type="button" class="btn btn-link nav-search navbar-toggle-box-collapse d-md-none" data-toggle="collapse"
              data-target="#navbarTogglerDemo01" aria-expanded="false">
              <span class="fa fa-search" aria-hidden="true"></span>
            </button>
            <div class="navbar-collapse collapse justify-content-center" id="navbarDefault">
              <ul class="navbar-nav">
                <!--<li class="nav-item">
                  <a class="nav-link active" href="<?= $this->makeURL(); ?>">Inicio</a>
                </li>-->
                <li class="nav-item">
                  <a class="nav-link" href="<?= $this->makeURL("nosotros"); ?>">Nosotros</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= $this->makeURL("contacto"); ?>">Contactanos</a>
                </li>
                <?php if (!isset($this->user)){ ?>
                <li class="nav-item">
                  <a class="nav-link" href="<?= $this->makeURL("hogares"); ?>">Hogares</a>
                </li>
                <?php } ?>
                <!--<li class="nav-item">
                  <a class="nav-link" href="blog-grid.html">Blog</a>
                </li>-->
                <!--<li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    Pages
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="property-single.html">Property Single</a>
                    <a class="dropdown-item" href="blog-single.html">Blog Single</a>
                    <a class="dropdown-item" href="agents-grid.html">Agents Grid</a>
                    <a class="dropdown-item" href="agent-single.html">Agent Single</a>
                  </div>
                </li>-->
                
                <?php if (isset($this->user)){ ?>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    Necesito
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="<?= $this->makeURL("alquilar"); ?>">Ganar dinero</a>
                    <a class="dropdown-item" href="<?= $this->makeURL("hogares"); ?>">Ver casas</a>
                  </div>
                </li>  

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    <?= $this->escapeHTML($this->user->nombre); ?>
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="<?= $this->makeURL("panel"); ?>">Panel de control</a>
                    <a class="dropdown-item" href="<?= $this->makeURL("profile"); ?>">Mi perfil</a>
                    <a class="dropdown-item" href="<?= $this->makeURL("login/logout"); ?>">Salir</a>
                  </div>
                </li>
                 <?php }else{ ?>
                <li class="nav-item">
                  <a class="nav-link" href="<?= $this->makeURL("login"); ?>">Ingresar</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= $this->makeURL("register"); ?>">Registrarse</a>
                </li>
                 <?php } ?>
              </ul>
            </div>
            <button type="button" class="btn btn-b-n navbar-toggle-box-collapse d-none d-md-block" data-toggle="collapse"
              data-target="#navbarTogglerDemo01" aria-expanded="false">
              <span class="fa fa-search" aria-hidden="true"></span>
            </button>
          </div>
        </nav>
        <!--/ Nav End /-->
        <div id="wrapper">
            <!--<div id="navbar" class="navbar navbar-default" style="background-color: #f06292; border-bottom-color: #f48fb1">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar" aria-expanded="false">
                            <span class="sr-only">Menu</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?= $this->makeURL(); ?>"><?= $this->escapeHTML(APP_NAME); ?></a>
                    </div>
                    <div id="main-navbar" class="collapse navbar-collapse"></div>
                     /#main-navbar
                </div>
            </div>-->
            <!-- /#navbar -->
            
            <div id="container">
                <div id="header"></div>
                <!-- /#header -->
                <div id="content">
                  
                    <div id="feedback" class="container">
                        <?php if (($danger = Flash::danger())): ?>
                          
                            <div class="alert alert-danger" style="margin-top: 120px" role="alert"><strong>Oh snap!</strong> <?= $this->escapeHTML($danger); ?></div>
                            <?php
                        endif;
                        if (($info = Flash::info())):
                            ?>
                            
                            <div class="alert alert-info" style="margin-top: 120px" role="alert"><strong>Heads up!</strong> <?= $this->escapeHTML($info); ?></div>
                            <?php
                        endif;
                        if (($success = Flash::success())):
                            ?>
                            
                            <div class="alert alert-success" style="margin-top: 120px" role="alert"><strong>Success!</strong> <?= $this->escapeHTML($success); ?></div>
                            <?php
                        endif;
                        if (($warning = Flash::warning())):
                            ?>
                           
                            <div class="alert alert-warning" style="margin-top: 120px" role="alert"><strong>Warning!</strong> <?= $this->escapeHTML($warning); ?></div>
                            <?php
                        endif;
                        if (($errors = Flash::session(Config::get("SESSION_ERRORS")))):
                            ?>
                            
                            <div class="alert alert-danger" style="margin-top: 120px" role="alert">
                                <strong>Error:</strong>
                                <ul>
                                    <?php foreach ($errors as $key => $values): ?>
                                        <?php foreach ($values as $value): ?>
                                            <li><?= $this->escapeHTML($value); ?></li>
                                        <?php endforeach; ?>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>
                  

