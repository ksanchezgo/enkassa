
<section class="intro-single">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-4"></div>
            <div class="col-md-4 col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Iniciar Sesión</h3>
                    </div>
                    <br>
                    <div class="panel-body">
                        <form class="form-a" action="<?= $this->makeUrl("login/_login"); ?>" method="post">
                            <div class="form-group">
                                
                                <input type="text" id="email-input" class="form-control form-control-lg form-control-a" name="correo" placeholder="Correo electrónico"/>
                            </div>
                            <div class="form-group">
                                
                                <input type="password" id="password-input" class="form-control form-control-lg form-control-a" name="contrasena" placeholder="Contraseña" />
                            </div>
                            <div class="checkbox">
                                <label for="remember">
                                    <input type="checkbox" id="remember" name="remember" /> Mantener sesión
                                </label>
                            </div>
                            <input type="hidden" name="csrf_token" value="<?php echo App\Utility\Token::generate(); ?>" />
                            <br>
                            <button type="submit" class="btn btn-b">Ingresar</button>
                            <a href="<?= $this->makeURL("register"); ?>" class="btn">Registrarse</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!--se abre en el header antes de las cajas de mensajes-->